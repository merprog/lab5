﻿using System;

namespace InterfaceLibrary
{
    public interface ICalc
    {
        double Calculate(double[] numbers);
    }
}
