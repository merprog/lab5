﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using InterfaceLibrary;

namespace MainApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ICalc calculator = LoadCalculator();
            if (calculator != null)
            {
                // Запуск безперервного обчислення у новому потоці
                Thread calculationThread = new Thread(() => ContinuousCalculation(calculator));
                calculationThread.Start();

                // Затримка закриття консолі
                Console.WriteLine("Press any key to exit...");
                Console.ReadLine();

                // Завершення обчислювального потоку
                calculationThread.Abort();
                UnloadLibrary();
            }
            else
            {
                Console.WriteLine("Calculator could not be loaded.");
            }
        }

        static void ContinuousCalculation(ICalc calculator)
        {
            Random random = new Random();
            while (true)
            {
                double[] numbers = GenerateRandomNumbers(random, 5, 1.0, 10.0); // Генерація 5 випадкових чисел від 1 до 10
                double result = calculator.Calculate(numbers);
                Console.WriteLine($"Result: {result}");
                Thread.Sleep(1000); // Пауза на 1 секунду між обчисленнями
            }
        }

        static double[] GenerateRandomNumbers(Random random, int count, double minValue, double maxValue)
        {
            double[] numbers = new double[count];
            for (int i = 0; i < count; i++)
            {
                numbers[i] = random.NextDouble() * (maxValue - minValue) + minValue;
            }
            return numbers;
        }

        static ICalc LoadCalculator()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CalcLib.dll");
            if (File.Exists(path))
            {
                try
                {
                    Assembly calculationAssembly = Assembly.LoadFrom(path);
                    Type calculatorType = calculationAssembly.GetType("CalcLib.Calculator");
                    if (calculatorType != null)
                    {
                        return (ICalc)Activator.CreateInstance(calculatorType);
                    }
                    else
                    {
                        Console.WriteLine("Calculator type not found.");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error loading assembly: {ex.Message}");
                }
            }
            else
            {
                Console.WriteLine("CalcLib.dll not found.");
            }
            return null;
        }

        static void UnloadLibrary()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CalcLib.dll");
            if (File.Exists(path))
            {
                IntPtr hModule = GetModuleHandle("CalcLib.dll");
                if (hModule != IntPtr.Zero)
                {
                    FreeLibrary(hModule);
                    Console.WriteLine("Library unloaded.");
                }
                else
                {
                    Console.WriteLine("Library not found in memory.");
                }
            }
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool FreeLibrary(IntPtr hModule);
    }
}
