﻿using System;
using System.Linq;
using System.Threading.Tasks;
using InterfaceLibrary;
using Newtonsoft.Json;

namespace CalcLib
{
    public class Calculator : ICalc
    {
        public double Calculate(double[] numbers)
        {
            double result = 0;
            object lockObj = new object();

            Parallel.ForEach(numbers, (number) =>
            {
                double[] sortedNumbers = QuickSort(numbers);
                double processedNumber = ProcessNumber(sortedNumbers);
                lock (lockObj)
                {
                    result += processedNumber;
                }
            });

            string jsonResult = JsonConvert.SerializeObject(new { Result = result });
            Console.WriteLine($"Serialized Result: {jsonResult}");

            return result;
        }

        private double ProcessNumber(double[] sortedNumbers)
        {
            // Простий обчислювальний алгоритм, наприклад, обчислення суми квадратів
            return sortedNumbers.Sum(x => Math.Pow(x, 2));
        }

        private double[] QuickSort(double[] array)
        {
            if (array.Length <= 1) return array;

            double pivot = array[array.Length / 2];
            double[] left = array.Where(x => x < pivot).ToArray();
            double[] right = array.Where(x => x > pivot).ToArray();

            return QuickSort(left).Concat(new double[] { pivot }).Concat(QuickSort(right)).ToArray();
        }
    }
}
